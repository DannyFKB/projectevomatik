import { Component, OnInit } from '@angular/core';
declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit {
  constructor() { }
  ngOnInit() { }
  private showLA() {
  	$(".divLA").show();
  	$(".divJS").hide();
  	$(".divJQ").hide();
  	$(".divCSS").hide();
  }
  private showJS() {
  	$(".divJS").show();
  	$(".divLA").hide();
  	$(".divJQ").hide();
  	$(".divCSS").hide();
  }
  private showJQ() {
  	$(".divJQ").show();
  	$(".divLA").hide();
  	$(".divJS").hide();
  	$(".divCSS").hide();
  }
  private showCSS() {
  	$(".divCSS").show();
  	$(".divLA").hide();
  	$(".divJS").hide();
  	$(".divJQ").hide();
  }
}
