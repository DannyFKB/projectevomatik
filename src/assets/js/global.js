(function($) {

$(function() {
$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      hover: true,
      belowOrigin: true,
}
	);
});

$(document).ready(function() {
	$('select').material_select();
});

})(jQuery);

